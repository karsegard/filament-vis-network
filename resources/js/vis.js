import { Network } from 'vis-network';
import { DataSet } from 'vis-data';


document.addEventListener("alpine:init", () => {
    console.log('alpine vis init');
    Alpine.data(
        "vis_network",
        ({
            state,
            statePath,
        }) => ({
            state: state,
            statePath: statePath,
            init() {
                console.log(state);
                var nodes = new DataSet(this.state.nodes);

                // create an array with edges
                var edges = new DataSet([
                  
                ]);

                // create a network
                var container = this.$el;
                var data = {
                    nodes: nodes,
                    edges: edges,
                };
                var options = {nodes: {
                    shape: "box",
                    
                  },};
                var network = new Network(container, data, options);

                this.$watch('state', (newState) => {
                    console.log(newState);
                  //  if (this.state !== newState) {
                        const nodes = new DataSet(newState.nodes);
                        const edges = new DataSet(newState.edges);
                        const data = {
                            nodes, edges
                        }
                        console.log(data);

                        network = new Network(container, data, options);
                  //  }
                });
            }
        })
    );
    console.log(Network, DataSet);
});