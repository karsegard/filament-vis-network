let mix = require("laravel-mix");

mix
.setPublicPath('./')
.js("resources/js/vis.js", "assets/js")
.version();